package info.sapifleamarket;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import info.sapifleamarket.adapter.AdListOnClickListener;
import info.sapifleamarket.adapter.MyAdAdapter;
import info.sapifleamarket.model.Ad;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";

    //retrieving an instance of the database and the reference to the location we want to write
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = database.getReference();

    //FirebaseStorage
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageRef = storage.getReference();

    //recyclerview
    private RecyclerView recyclerView;

    View root;

    //ads adapter
    private MyAdAdapter myAdAdapter;

    //list of ads
    private List<Ad> adList = new ArrayList<>();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(root == null) {
            // Inflate the layout for this fragment
            root = inflater.inflate(R.layout.fragment_home, container, false);
        }

        recyclerView = (RecyclerView) root.findViewById(R.id.lv_ads);

        downloadAdsFromDB();

        initializeRecyclerView();
        //initializing the recyclerView
        //initializeRecyclerView(root);

        //myAdAdapter.notifyDataSetChanged();

        Log.d(TAG, "onCreateView finished");

        return root;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void initializeRecyclerView() {
        Log.d(TAG, "initializeRecyclerView");

        myAdAdapter = new MyAdAdapter(adList, new AdListOnClickListener() {
            @Override
            public void onItemClicked(String key) {
                //Toast.makeText(getContext(), "Clicked on: " + key, Toast.LENGTH_SHORT).show();

                //opening the home fragment
                Fragment adDetailPageFragment = AdDetailPageFragment.newInstance(key);
                getActivity().getSupportFragmentManager().beginTransaction()
                 .replace(R.id.fragment_container, adDetailPageFragment, "findThisFragment")
                                  .addToBackStack(null).commit();
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
    }


    private void downloadAdsFromDB() {
        Log.d(TAG, "fuggveny meghivodva");

        databaseReference.child("ads").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.getChildren() != null) {
                    for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                        //Log.d(TAG, "DELETE" + singleSnapshot.child("isDeleted").getValue().toString());
                        if (singleSnapshot.child("isDeleted").getValue().toString().equals("false")){
                            String title = new String();
                            String shortDescription = new String();
                            String imageURL = new String();
                            int numberOfViews = 0;

                            title = singleSnapshot.child("title").getValue().toString();
                            shortDescription = singleSnapshot.child("shortDescription").getValue().toString();
                            numberOfViews = Integer.valueOf(singleSnapshot.child("numberOfView").getValue().toString());
                            //add error handling in case of null
                            imageURL = singleSnapshot.child("imageStorageIDs").child("0").getValue().toString();

                            Ad ad = new Ad(singleSnapshot.getKey(), title, shortDescription, numberOfViews, imageURL);
                            adList.add(ad);
                        }
                    }
                }

                for (int i = 0; i < adList.size(); i++) {
                    Log.d(TAG, "Ad's details:" + adList.get(i).getTitle() + " " + adList.get(i).getShortDescription() + " " + adList.get(i).getNumberOfView() + " " + adList.get(i).getImageURL());
                }


                recyclerView.setAdapter(myAdAdapter);
                myAdAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }


}
