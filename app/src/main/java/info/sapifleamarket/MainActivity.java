package info.sapifleamarket;
//ms.sapientia.ro.sapifleamarket

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;

import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{

    private static final String TAG = "MainActivity";
    private static final int RC_SIGN_IN = 1;

    //retrieving an instance of the database and the reference to the location we want to write
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = database.getReference();

    //FirebaseStorage
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReference();


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //initializing the BottomNavigationView
        BottomNavigationView bottomNavigationVIew = (BottomNavigationView) findViewById(R.id.navigationView);

        bottomNavigationVIew.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener()
                {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item)
                    {

                        Fragment fragment = null;

                        switch (item.getItemId()) {
                            case R.id.navigation_add:
                                Log.d(TAG, "Add");
                                //creating and launching the fragment
                                fragment = new AddNewAdFragment();
                                break;
                            case R.id.navigation_home:
                                Log.d(TAG, "HomeFragment");
                                fragment = new HomeFragment();
                                break;
                            case R.id.navigation_profile:
                                Log.d(TAG, "Profile");
                                fragment = new MyProfileFragment();
                                break;
                        }
                        loadFragment(fragment);
                        return false;
                    }
                }
        );


    }


    @Override
    public void onStart()
    {
        super.onStart();

        //Start sign in if necessary
        if (shouldStartSignIn()) {
            startSignIn();
            return;
        }
//        else {
//            loadFragment(new HomeFragment());
//        }
    }

    private boolean shouldStartSignIn()
    {
        return (FirebaseAuth.getInstance().getCurrentUser() == null);
    }

    private void startSignIn()
    {
        Intent intent = AuthUI.getInstance().createSignInIntentBuilder().
                setAvailableProviders(Collections.singletonList(new AuthUI.IdpConfig.PhoneBuilder().build())).
                setIsSmartLockEnabled(false).build();

        startActivityForResult(intent, RC_SIGN_IN);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {

                // Successfully signed in
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                Log.d(TAG, "user: " + user.toString());
                Log.d(TAG, "UID: " + FirebaseAuth.getInstance().getUid());

                Log.v(TAG, "Logged In");

                checkIfUserRegistered();

            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
                if (response == null) {
                    Toast.makeText(getApplicationContext(), "Login was cancelled by pressing Back button", Toast.LENGTH_LONG).show();
                } else {
                    Log.d(TAG, response.getError().toString());
                }

                Log.v(TAG, "Login unsuccessful");

            }
        }
    }

    private void checkIfUserRegistered()
    {

        Query userIDQuery = databaseReference.child("users");

        userIDQuery.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                final ArrayList<String> phoneNumbers = new ArrayList<>();
                boolean userIsRegistered = false;
                Log.d(TAG, "onDataChange");

                if(dataSnapshot.getChildren() != null)
                {
                    for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                        String phoneNumber = singleSnapshot.child("phoneNumber").getValue().toString();
                        Log.d(TAG, "singleSnapshot.phoneNumber: " + phoneNumber);
                        phoneNumbers.add(phoneNumber);
                    }

                    for (int i = 0; i < phoneNumbers.size(); i++) {
                        if (phoneNumbers.get(i).equals(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber())) {
                            userIsRegistered = true;
                            break;
                        }
                    }

                    if (!userIsRegistered) {
                        Log.d(TAG, "user should register");
                        Fragment fragment = new RegisterFragment();
                        loadFragment(fragment);
                    } else {
                        Log.d(TAG, "user is registered");

                        loadFragment(new HomeFragment());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Log.e(TAG, "onCancelled", databaseError.toException());
            }
        });

    }

    private boolean loadFragment(Fragment fragment)
    {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, fragment).addToBackStack(null)
                    .commit();
            return true;
        }

        return false;
    }


}

