package info.sapifleamarket;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import info.sapifleamarket.adapter.AdListOnClickListener;
import info.sapifleamarket.adapter.MyAdAdapter;
import info.sapifleamarket.model.Ad;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyAdsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyAdsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyAdsFragment extends Fragment {
    private static final String TAG = "MyAdsFragment";

    //retrieving an instance of the database and the reference to the location we want to write
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = database.getReference();

    //FirebaseStorage
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageRef = storage.getReference();

    //recyclerview
    private RecyclerView recyclerView;

    //recyclerview
    private RecyclerView myRecyclerView;

    //ads adapter
    private MyAdAdapter myAdAdapter;

    //list of ads
    private List<Ad> myAdList = new ArrayList<>();

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public MyAdsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyAdsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyAdsFragment newInstance(String param1, String param2) {
        MyAdsFragment fragment = new MyAdsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_my_ads, container, false);
        myRecyclerView=(RecyclerView) root.findViewById(R.id.lv_my_ads);
        downloadMyAdsFromDB();
        initializeMyRecyclerView();
        return root;
    }

    private void downloadMyAdsFromDB() {
        Log.d(TAG, "My ads");

        databaseReference.child("ads").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //User user = dataSnapshot.getValue(User.class);
                if (dataSnapshot.getChildren() != null) {
                    for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                        Log.d(TAG, "isDeleted:" + singleSnapshot.child("title").toString() + singleSnapshot.child("isDeleted").getValue().toString());
                        if(getUserID().equals(singleSnapshot.child("userID").getValue().toString()) && (singleSnapshot.child("isDeleted").getValue().toString().equals("false"))) {
                                String title = new String();
                                String shortDescription = new String();
                                String imageURL = new String();
                                int numberOfViews = 0;
                                title = singleSnapshot.child("title").getValue().toString();
                                shortDescription = singleSnapshot.child("shortDescription").getValue().toString();
                                numberOfViews = Integer.valueOf(singleSnapshot.child("numberOfView").getValue().toString());
                                //add error handling in case of null
                                imageURL = singleSnapshot.child("imageStorageIDs").child("0").getValue().toString();

                                Ad ad = new Ad(singleSnapshot.getKey(), title, shortDescription, numberOfViews, imageURL);
                                myAdList.add(ad);

                        }

                    }
                }

                for (int i = 0; i < myAdList.size(); i++) {
                    Log.d(TAG, "Ad's details:" + myAdList.get(i).getTitle() + " " + myAdList.get(i).getShortDescription() + " " + myAdList.get(i).getNumberOfView() + " " + myAdList.get(i).getImageURL());
                }


                myRecyclerView.setAdapter(myAdAdapter);
                myAdAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    private String getUserID()
    {
        String user = FirebaseAuth.getInstance().getUid();
        String userID = user.substring(user.lastIndexOf(".") + 1);
        Log.d(TAG, userID);

        return userID;
    }
    private void initializeMyRecyclerView() {
//        Log.d(TAG, "initializeMyRecyclerView");
//
//        myAdAdapter = new MyAdAdapter(myAdList, new AdListOnClickListener() {
//            @Override
//            public void onItemClicked(String key) {
//                Toast.makeText(getContext(), "Clicked on: " + key, Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
//        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
//        myRecyclerView.setLayoutManager(mLayoutManager);


        Log.d(TAG, "initializeRecyclerView");

        myAdAdapter = new MyAdAdapter(myAdList, new AdListOnClickListener() {
            @Override
            public void onItemClicked(String key) {
                //Toast.makeText(getContext(), "Clicked on: " + key, Toast.LENGTH_SHORT).show();

                //opening the detail page fragment
                Fragment myAdDetailPageFragment = MyAdDetailPageFragment.newInstance(key);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, myAdDetailPageFragment, "findThisFragment")
                        .addToBackStack(null).commit();
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        myRecyclerView.setLayoutManager(mLayoutManager);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
