package info.sapifleamarket;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyProfileFragment extends Fragment implements View.OnClickListener
{

    private static final String TAG = "MyProfileFragment";
    private static final int START_ACTIVITY = 1;

    //retrieving an instance of the database and the reference to the location we want to write
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = database.getReference();

    EditText etFirstName, etLastName, etPhoneNumber;
    Button editProfile, signOut, saveFields;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public MyProfileFragment()
    {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyProfileFragment newInstance(String param1, String param2)
    {
        MyProfileFragment fragment = new MyProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_profile, container, false);

        downloadUserData(getUserID());

        etFirstName = (EditText) v.findViewById(R.id.myProfileFirstName);
        etFirstName.setEnabled(false);

        etLastName = (EditText) v.findViewById(R.id.myProfileLastName);
        etLastName.setEnabled(false);

        etPhoneNumber = (EditText) v.findViewById(R.id.myProfilePhoneNumber);
        etPhoneNumber.setEnabled(false);

        editProfile = (Button) v.findViewById(R.id.myProfileEdit);
        editProfile.setOnClickListener(this);

        signOut = (Button) v.findViewById(R.id.myProfileLogout);
        signOut.setOnClickListener(this);


        saveFields = (Button) v.findViewById(R.id.myProfileSave);
        saveFields.setOnClickListener(this);

        Button myAds = (Button) v.findViewById(R.id.myProfileMyAds);
        myAds.setOnClickListener(this);


        return v;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public void onClick(View view)
    {
        MyAdsFragment nextfragment = new MyAdsFragment();
        switch (view.getId())
        {
            case R.id.myProfileLogout:
                FirebaseAuth.getInstance().signOut();
                Log.d(TAG, "logout pressed");
                startActivity(new Intent(getActivity(), MainActivity.class));
                break;
            case R.id.myProfileEdit:
                editFields();
                break;
            case R.id.myProfileSave:
                Log.d(TAG, "save pressed");
                saveFields();
                break;
            case R.id.myProfileMyAds:
                Log.d(TAG, "MyADS FRagment: " + nextfragment);
                FragmentTransaction fragmentTransaction=getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, nextfragment).addToBackStack(null);
                        fragmentTransaction.commit();
                break;
            default:
                break;
        }


    }

    private void editFields()
    {
        etFirstName.setEnabled(true);
        etLastName.setEnabled(true);
        etPhoneNumber.setEnabled(true);
    }

    private void saveFields()
    {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String phoneNumber = etPhoneNumber.getText().toString();

        Log.d(TAG, "new values: "+ firstName + " " + lastName + " "+ phoneNumber);

        databaseReference.child("users").child(getUserID()).child("firstName").setValue(firstName);
        databaseReference.child("users").child(getUserID()).child("lastName").setValue(lastName);
        databaseReference.child("users").child(getUserID()).child("phoneNumber").setValue(phoneNumber);

        etFirstName.setEnabled(false);
        etLastName.setEnabled(false);
        etPhoneNumber.setEnabled(false);
    }

    private String getUserID()
    {
        String user = FirebaseAuth.getInstance().getUid();
        String userID = user.substring(user.lastIndexOf(".") + 1);
        Log.d(TAG, "getUserID(): " + userID);

        return userID;
    }

    private void downloadUserData(String userID)
    {
        final Query userIDQuery = databaseReference.child("users").child(userID);

        userIDQuery.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                final ArrayList<String> dataValues = new ArrayList<>();

                Log.d(TAG, "onDataChange");

                if(dataSnapshot.getChildren() != null)
                {
                    for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                            String value = singleSnapshot.getValue().toString();
                            Log.d(TAG, "singleSnapshot.value: " + value);
                            dataValues.add(value);
                    }

                    EditText etFirstName = getView().findViewById(R.id.myProfileFirstName);
                    etFirstName.setText(dataValues.get(0));
                    EditText etLastName = getView().findViewById(R.id.myProfileLastName);
                    etLastName.setText(dataValues.get(1));
                    EditText etPhoneNumber = getView().findViewById(R.id.myProfilePhoneNumber);
                    etPhoneNumber.setText(dataValues.get(2));

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Log.e(TAG, "onCancelled", databaseError.toException());
            }
        });
    }


}
