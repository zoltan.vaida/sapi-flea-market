package info.sapifleamarket;

import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ShareActionProvider;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AdDetailPageFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AdDetailPageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AdDetailPageFragment extends Fragment implements  View.OnClickListener{

    private static final String TAG = "AdDetailPageFragment";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String KEY = "key";

    //retrieving an instance of the database and the reference to the location we want to write
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = database.getReference();

    private int indexOfImageShown = 0;
    List<String> imageURLs = new ArrayList<>();
    private ImageView imageView;
    private Button prv, nxt, myShareAd;
    String  numberOfViews;

    String shareBody = "";


    // TODO: Rename and change types of parameters
    private String key;


    public AdDetailPageFragment() {
        // Required empty public constructor
    }

    /*
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AdDetailPageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AdDetailPageFragment newInstance(String key) {
        AdDetailPageFragment fragment = new AdDetailPageFragment();
        Bundle args = new Bundle();
        args.putString(KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            key = getArguments().getString(KEY);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ad_detail_page, container, false);

        prv = (Button) view.findViewById(R.id.previousDP);
        prv.setOnClickListener(this);

        nxt = (Button) view.findViewById(R.id.nextDP);
        nxt.setOnClickListener(this);

        myShareAd = (Button) view.findViewById(R.id.detailPageShare);
        myShareAd.setOnClickListener(this);

        downloadData(key);
        setButtonStates();

        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    public void downloadData(String key)
    {
        Query userIDQuery = databaseReference.child("ads").child(key);

        userIDQuery.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Log.d(TAG, "onDataChange");

                if(dataSnapshot.getChildren() != null)
                {

                    String title = new String();
                    String longDescription = new String();
                    String phoneNumber = new String();
                    String location = new String();


                    //add error handling in case of null
                    for(int i = 0; i < dataSnapshot.child("imageStorageIDs").getChildrenCount(); i++)
                    {
                        imageURLs.add(dataSnapshot.child("imageStorageIDs").child(i + "").getValue().toString());

                    }
                    Log.d(TAG, "imageURLs: " + imageURLs);
                    location = dataSnapshot.child("location").getValue().toString();
                    longDescription = dataSnapshot.child("longDescription").getValue().toString();
                    phoneNumber = dataSnapshot.child("phoneNumber").getValue().toString();
                    title = dataSnapshot.child("title").getValue().toString();

                    numberOfViews = dataSnapshot.child("numberOfView").getValue().toString();
                    Log.d(TAG, "lekerdezes numberOfView: " + numberOfViews);


                    imageView = getView().findViewById(R.id.detailPageImageView);
                    Glide.with(getContext())
                            .load(imageURLs.get(indexOfImageShown))
                            .into(imageView);
                    TextView tvTitle = getView().findViewById(R.id.detailPageTitle);
                    tvTitle.setText(title);
                    TextView tvLongDescription = getView().findViewById(R.id.detailPageLongDescription);
                    tvLongDescription.setText(longDescription);
                    TextView tvPhoneNumber = getView().findViewById(R.id.detailPagePhoneNumber);
                    tvPhoneNumber.setText(phoneNumber);
                    TextView tvLocation = getView().findViewById(R.id.detailPageLocation);
                    tvLocation.setText(location);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Log.e(TAG, "onCancelled", databaseError.toException());
            }
        });

        //increasing the number of views
        int increasedNumber=0;
        try{
            increasedNumber = Integer.parseInt(numberOfViews);

        }catch (NumberFormatException e)
        {
            Log.d(TAG, e.getMessage());
        }

        increasedNumber++;
        Log.d(TAG, "increasedNumber: " + increasedNumber);
        databaseReference.child("ads").child(key).child("numberOfView").setValue(increasedNumber);
    }


    private int nextImage()
    {
        if(indexOfImageShown < imageURLs.size()-1)
        {
            indexOfImageShown++;
            setButtonStates();
            Glide.with(getContext())
                    .load(imageURLs.get(indexOfImageShown))
                    .into(imageView);
            Log.d(TAG, "Image currently shown: " + imageURLs.get(indexOfImageShown));
        }
        else if(indexOfImageShown == imageURLs.size()-1)
        {
            setButtonStates();
            Glide.with(getContext())
                    .load(imageURLs.get(indexOfImageShown))
                    .into(imageView);
            Log.d(TAG, "Image currently shown: " + imageURLs.get(indexOfImageShown));
        }

        return indexOfImageShown;
    }


    private int previousImage()
    {
        if(indexOfImageShown > 0)
        {
            indexOfImageShown--;
            setButtonStates();
            Glide.with(getContext())
                    .load(imageURLs.get(indexOfImageShown))
                    .into(imageView);
            Log.d(TAG, "Image currently shown: " + imageURLs.get(indexOfImageShown));
        }
        else if(indexOfImageShown == 0)
        {
            setButtonStates();
            Glide.with(getContext())
                    .load(imageURLs.get(indexOfImageShown))
                    .into(imageView);
            Log.d(TAG, "Image currently shown: " + imageURLs.get(indexOfImageShown));
        }

        return indexOfImageShown;
    }

    private void setButtonStates()
    {
            if(indexOfImageShown == 0)
            {
                prv.setEnabled(false);
                nxt.setEnabled(true);
            }
            else if(indexOfImageShown == imageURLs.size()-1)
            {
                prv.setEnabled(true);
                nxt.setEnabled(false);
            }
            else
            {
                prv.setEnabled(true);
                nxt.setEnabled(true);
            }
    }

    private void shareAd()
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        Query userIDQuery = databaseReference.child("ads").child(key);

        userIDQuery.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.getChildren() != null)
                {

                    String title = new String();
                    String shortDescription = new String();
                    String phoneNumber = new String();
                    String location = new String();



                    location = dataSnapshot.child("location").getValue().toString();
                    shortDescription = dataSnapshot.child("shortDescription").getValue().toString();
                    phoneNumber = dataSnapshot.child("phoneNumber").getValue().toString();
                    title = dataSnapshot.child("title").getValue().toString();

                    shareBody = title + " " + shortDescription + " " + location + " " + phoneNumber;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Log.e(TAG, "onCancelled", databaseError.toException());
            }
        });


        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
        Log.d(TAG, "shareAd() called and ran");
    }


    @Override
    public void onClick(View view) {
        Log.d(TAG, "onClick()");

        switch (view.getId()) {
            case R.id.previousDP:
                Log.d(TAG, "previous");
                indexOfImageShown = previousImage();
                break;
            case R.id.nextDP:
                Log.d(TAG, "next");
                indexOfImageShown = nextImage();
                break;
            case R.id.detailPageShare:
                Log.d(TAG, "share");
                shareAd();
                break;
            default:
                break;
        }

    }
}
