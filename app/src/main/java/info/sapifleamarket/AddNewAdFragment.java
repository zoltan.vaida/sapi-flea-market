package info.sapifleamarket;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import info.sapifleamarket.model.Ad;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddNewAdFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddNewAdFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddNewAdFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "AddNewAdFragment";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    //retrieving an instance of the database and the reference to the location we want to write
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = database.getReference();

    //FirebaseStorage
    private FirebaseStorage storage = FirebaseStorage.getInstance();


    int numberOfImagesUploaded = 0;

    //Add button - for saving the entered data to the database
    Button saveButton, addImageButton, addFromCamera;

    //image switcher
    private ImageSwitcher imageSwitcher;
    private Button prv, nxt;
    private static final int PICK_IMAGE_REQUEST = 2;
    private static final int CAMERA_REQUEST = 1;
    private Uri mImageUri;
    private ArrayList<Uri> imageUriList = new ArrayList<>();

    //loading Spinner
    private ProgressBar spinner;
    private int indexOfImageShown = 0;




    private OnFragmentInteractionListener mListener;

    public AddNewAdFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddNewAdFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddNewAdFragment newInstance(String param1, String param2) {
        AddNewAdFragment fragment = new AddNewAdFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add_new_ad, container, false);

        saveButton = (Button) v.findViewById(R.id.save_ad);
        saveButton.setOnClickListener(this);

        addImageButton = (Button) v.findViewById(R.id.add_image);
        addImageButton.setOnClickListener(this);

        addFromCamera = (Button) v.findViewById(R.id.add_image_from_camera);
        addFromCamera.setOnClickListener(this);

        prv = (Button) v.findViewById(R.id.previous);
        prv.setOnClickListener(this);

        nxt = (Button) v.findViewById(R.id.next);
        nxt.setOnClickListener(this);

        imageSwitcher = (ImageSwitcher) v.findViewById(R.id.addFragmentImageSwitcher);
        imageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView imageView = new ImageView(container.getContext());
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                return imageView;
            }
        });

        spinner = v.findViewById(R.id.loadingSpinner);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.add_image:
                openGallery();

                break;
            case R.id.previous:
                indexOfImageShown = previousImage();
                break;
            case R.id.next:
                indexOfImageShown = nextImage();
                break;
            case R.id.save_ad:
                Log.d(TAG, "save_ad clicked");

                EditText etTitle = getView().findViewById(R.id.addTitle);
                String title = etTitle.getText().toString();
                EditText etShortDescription = getView().findViewById(R.id.addShortDescription);
                String shortDescription = etShortDescription.getText().toString();
                EditText etLongDescription = getView().findViewById(R.id.addLongDescription);
                String longDescription = etLongDescription.getText().toString();
                EditText etPhoneNumber = getView().findViewById(R.id.addPhoneNumber);
                String phoneNumber = etPhoneNumber.getText().toString();
                EditText etLocation = getView().findViewById(R.id.addLocation);
                String location = etLocation.getText().toString();

                adNewAdToDB(title, shortDescription, longDescription,phoneNumber, location);
                break;
            case R.id.add_image_from_camera:
                openCamera();
                break;
            default:
                break;
        }

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void adNewAdToDB(final String title, final String shortDescription, final String longDescription, final String phoneNumber, final String location) {

        //start spinner
        spinner.setVisibility(View.VISIBLE);

       uploadImages(new OnUploadFinishedListener() {
           @Override
           public void uploadFinished(List<String> uris) {


               Log.d(TAG, "imagestorage id:" + uris.toString());

               Ad advertisement = new Ad(title, shortDescription, longDescription, phoneNumber, location, getUserID(), uris, 0, "false");
               Log.d(TAG, "ad details: " + advertisement.getIsDeleted());
               databaseReference.child("ads").child(createAdID()).setValue(advertisement);

               EditText etTitle = getView().findViewById(R.id.addTitle);
               etTitle.setText(null);
               EditText etShortDescription = getView().findViewById(R.id.addShortDescription);
               etShortDescription.setText(null);
               EditText etLongDescription = getView().findViewById(R.id.addLongDescription);
               etLongDescription.setText(null);
               EditText etPhoneNumber = getView().findViewById(R.id.addPhoneNumber);
               etPhoneNumber.setText(null);
               EditText etLocation = getView().findViewById(R.id.addLocation);
               etLocation.setText(null);
               ImageSwitcher imageSwitcher = getView().findViewById(R.id.addFragmentImageSwitcher);
               imageSwitcher.setImageURI(null);
           }
       });


    }


    private void uploadImages(final OnUploadFinishedListener listener)
    {
      // StorageReference adReference = storageRef.child(createAdID());
      // String imageStorageID = adReference.toString().substring(adReference.toString().lastIndexOf("/") + 1);

        final List <String> imageStorageIDs=new ArrayList<>();

        if(imageUriList.size() > 0)
        {
            for(int i =0 ; i<imageUriList.size(); i++)
            {

                final Uri file = imageUriList.get(i);
                //StorageReference imageReference = adReference.child(file.getLastPathSegment());
                final StorageReference imageReference =  storage.getReference("images/" + System.currentTimeMillis()+".jpg");


                final UploadTask uploadTask = imageReference.putFile(file);

                Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        numberOfImagesUploaded++;
                        // Continue with the task to get the download URL
                        Log.d(TAG, "Storageref:" +imageReference.getDownloadUrl());
                        return imageReference.getDownloadUrl();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "Upload unsuccsessful " + e.toString());
                        Toast.makeText(getView().getContext(), "Uploading images unsuccsessful", Toast.LENGTH_LONG).show();
//                        numberOfImagesUploaded++;
                        if(numberOfImagesUploaded == imageUriList.size())
                        {
                            //hide spinner
                            spinner.setVisibility(View.GONE);
                            listener.uploadFinished(imageStorageIDs);
                        }
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            Uri downloadUri = task.getResult();

                            Log.d(TAG, "downloadURL: " + downloadUri);

                            imageStorageIDs.add(downloadUri.toString());
                        } else {
                            // Handle failures
                            // ...
                            Log.d(TAG, "downloadURL  else: ");
                       }

                        if(numberOfImagesUploaded == imageUriList.size())
                        {
                            //hide spinner
                            spinner.setVisibility(View.GONE);
                            listener.uploadFinished(imageStorageIDs);
                        }
                    }
                });
            }
        }

        //hide spinner
        spinner.setVisibility(View.GONE);
    }

    private int nextImage()
    {
        if(indexOfImageShown < imageUriList.size()-1)
        {
            indexOfImageShown++;
            setButtonStates();
            imageSwitcher.setImageURI(imageUriList.get(indexOfImageShown));
            Log.d(TAG, "Image currently shown: " + imageUriList.get(indexOfImageShown));
        }
        else if(indexOfImageShown == imageUriList.size()-1)
        {
            setButtonStates();
            imageSwitcher.setImageURI(imageUriList.get(indexOfImageShown));
            Log.d(TAG, "Image currently shown: " + imageUriList.get(indexOfImageShown));
        }

        return indexOfImageShown;
    }

    private int previousImage()
    {
        if(indexOfImageShown > 0)
        {
            indexOfImageShown--;
            setButtonStates();
            imageSwitcher.setImageURI(imageUriList.get(indexOfImageShown));
            Log.d(TAG, "Image currently shown: " + imageUriList.get(indexOfImageShown));
        }
        else if(indexOfImageShown == 0)
        {
            setButtonStates();
            imageSwitcher.setImageURI(imageUriList.get(indexOfImageShown));
            Log.d(TAG, "Image currently shown: " + imageUriList.get(indexOfImageShown));
        }

        return indexOfImageShown;
    }

    private void setButtonStates()
    {
        if(indexOfImageShown == 0)
        {
            prv.setEnabled(false);
            nxt.setEnabled(true);
        }
        else if(indexOfImageShown == imageUriList.size()-1)
        {
            prv.setEnabled(true);
            nxt.setEnabled(false);
        }
        else
        {
            prv.setEnabled(true);
            nxt.setEnabled(true);
        }
    }

    private String getUserID()
    {
        String user = FirebaseAuth.getInstance().getUid();
        String userID = user.substring(user.lastIndexOf(".") + 1);
        Log.d(TAG, userID);

        return userID;
    }

    private String createAdID()
    {
        long dtMili = System.currentTimeMillis();
        Log.d(TAG, String.valueOf(dtMili));

        String adID = dtMili + "_" + getUserID();

        return adID;
    }

    private void openGallery()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select images"), PICK_IMAGE_REQUEST);
    }

    private void openCamera()
    {
        Intent cameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");

        //startActivityForResult(cameraIntent, CAMERA_REQUEST);
        File file = new File(getActivity().getExternalCacheDir(),
                String.valueOf(System.currentTimeMillis()) + ".jpg");
        Uri fileUri = Uri.fromFile(file);
        String fileURI = fileUri.toString();
        cameraIntent.putExtra("fileURI", fileURI);
        getActivity().startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try{

            Log.d(TAG, requestCode + " " + resultCode);
            //When an Image is picked
            if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null) {

                if(data.getData() != null)
                {
                    mImageUri = data.getData();
                    Log.d(TAG, "mImageUri: " + mImageUri);
                    imageUriList.add(mImageUri);

                    imageSwitcher.setImageURI(imageUriList.get(indexOfImageShown));
                    Log.d(TAG, "Image currently shown: " + imageUriList.get(indexOfImageShown));
                }
                else
                {
                    if(data.getClipData() != null)
                    {
                        ClipData mClipData = data.getClipData();
                        Log.d(TAG, "mClipData: " + mClipData);

                        for(int i = 0; i < mClipData.getItemCount(); i++)
                        {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            imageUriList.add(uri);
                            Log.d(TAG, "URI: " + uri);
                        }

                        Log.d(TAG, "Selected Images: "+imageUriList.size());

                        Button next = getView().findViewById(R.id.next);
                        next.setEnabled(true);
                    }

                    imageSwitcher.setImageURI(imageUriList.get(indexOfImageShown));
                    Log.d(TAG, "Image currently shown: " + imageUriList.get(indexOfImageShown));
                }
            }
            else if(requestCode == CAMERA_REQUEST && resultCode == RESULT_OK)
            {
                Log.d(TAG, "getting back from camera");


                Bitmap mySelectedImage = (Bitmap) data.getExtras().get("fileURI");

                mImageUri = getImageUri(getContext(), mySelectedImage);
                imageUriList.add(mImageUri);

                imageSwitcher.setImageURI(imageUriList.get(indexOfImageShown));
                Log.d(TAG, "Image currently shown: " + imageUriList.get(indexOfImageShown));


//                Bundle bundle = new Bundle();
//                bundle = data.getExtras();
//
//                mImageUri = getImageUri(bundle.getString("fileURI"));
//
//                imageUriList.add(mImageUri);
//
//                imageSwitcher.setImageURI(imageUriList.get(indexOfImageShown));
//                Log.d(TAG, "Image currently shown: " + imageUriList.get(indexOfImageShown));
            }
            else //When no image is picked
            {
                Toast.makeText(getView().getContext(), "You haven't picked Image", Toast.LENGTH_LONG).show();
            }

        }catch(Exception e)
        {
            Toast.makeText(getView().getContext(), "Something went wrong", Toast.LENGTH_LONG).show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
