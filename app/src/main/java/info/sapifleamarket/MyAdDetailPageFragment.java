package info.sapifleamarket;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import info.sapifleamarket.adapter.MyAdAdapter;
import info.sapifleamarket.model.Ad;


public class MyAdDetailPageFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "MyAdDetailPageFragment";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String KEY = "key";

    //retrieving an instance of the database and the reference to the location we want to write
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = database.getReference();

    private int indexOfImageShown = 0;
    List<String> imageURLs = new ArrayList<>();
    private ImageView imageView;
    private Button prv, nxt, myShareAd, delete;
    private Ad adToBeRemoved;

    private String key;

    //ads adapter
    private MyAdAdapter myAdAdapter;

    //list of ads
    private List<Ad> myAdList = new ArrayList<>();

    //recyclerview
    private RecyclerView myRecyclerView;
    private TextView tvTitle;
    private TextView tvLongDescription;
    private TextView tvPhoneNumber;
    private TextView tvLocation;


    public MyAdDetailPageFragment() {
        // Required empty public constructor
    }

    public static MyAdDetailPageFragment newInstance(String key) {
        MyAdDetailPageFragment fragment = new MyAdDetailPageFragment();
        Bundle args = new Bundle();
        args.putString(KEY, key);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            key = getArguments().getString(KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_ad_detail_page, container, false);
        initialize(view);

        prv.setOnClickListener(this);
        nxt.setOnClickListener(this);
        myShareAd.setOnClickListener(this);
        delete.setOnClickListener(this);

        myDownloadData(key);
        setButtonStates();

        return view;
    }

    private void initialize(View view) {
        imageView = view.findViewById(R.id.myAdDetailPageImage);
        tvTitle = view.findViewById(R.id.myDetailPageTitle);
        tvLongDescription = view.findViewById(R.id.myAdDetailPageLongDescription);
        tvPhoneNumber = view.findViewById(R.id.myAdDetailPagePhoneNumber);
        tvLocation = view.findViewById(R.id.myAdDetailPageLocation);
        prv = (Button) view.findViewById(R.id.my_previousDP);
        nxt = (Button) view.findViewById(R.id.my_nextDP);
        myShareAd = (Button) view.findViewById(R.id.myAdDetailPageShare);
        delete= (Button) view.findViewById(R.id.myAdDetailPageDelete);


    }

    public void myDownloadData(String key)
    {
        Query userIDQuery = databaseReference.child("ads").child(key);

        userIDQuery.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Log.d(TAG, "onDataChange");

                if(dataSnapshot.getChildren() != null)
                {

                    String title = new String();
                    String longDescription = new String();
                    String phoneNumber = new String();
                    String location = new String();
                    String isDeleted = new String();
                    String numberOfViews= new String();
                    String shortDescription = new String();
                    String userID = new String();


                    //add error handling in case of null
                    for(int i = 0; i < dataSnapshot.child("imageStorageIDs").getChildrenCount(); i++)
                    {
                        imageURLs.add(dataSnapshot.child("imageStorageIDs").child(i + "").getValue().toString());

                    }
                   // Log.d(TAG, "imageURLs: " + imageURLs);
                    location = dataSnapshot.child("location").getValue().toString();
                    longDescription = dataSnapshot.child("longDescription").getValue().toString();
                    phoneNumber = dataSnapshot.child("phoneNumber").getValue().toString();
                    title = dataSnapshot.child("title").getValue().toString();

                    Glide.with(getContext())
                            .load(imageURLs.get(indexOfImageShown))
                            .into(imageView);
                    tvTitle.setText(title);
                    tvLongDescription.setText(longDescription);
                    tvPhoneNumber.setText(phoneNumber);
                    tvLocation.setText(location);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                Log.e(TAG, "onCancelled", databaseError.toException());
            }
        });
    }



    private int myNextImage()
    {
        if(indexOfImageShown < imageURLs.size()-1)
        {
            indexOfImageShown++;
            setButtonStates();
            Glide.with(getContext())
                    .load(imageURLs.get(indexOfImageShown))
                    .into(imageView);
            Log.d(TAG, "Image currently shown: " + imageURLs.get(indexOfImageShown));
        }
        else if(indexOfImageShown == imageURLs.size()-1)
        {
            setButtonStates();
            Glide.with(getContext())
                    .load(imageURLs.get(indexOfImageShown))
                    .into(imageView);
            Log.d(TAG, "Image currently shown: " + imageURLs.get(indexOfImageShown));
        }

        return indexOfImageShown;
    }


    private int myPreviousImage()
    {
        if(indexOfImageShown > 0)
        {
            indexOfImageShown--;
            setButtonStates();
            Glide.with(getContext())
                    .load(imageURLs.get(indexOfImageShown))
                    .into(imageView);
            Log.d(TAG, "Image currently shown: " + imageURLs.get(indexOfImageShown));
        }
        else if(indexOfImageShown == 0)
        {
            setButtonStates();
            Glide.with(getContext())
                    .load(imageURLs.get(indexOfImageShown))
                    .into(imageView);
            Log.d(TAG, "Image currently shown: " + imageURLs.get(indexOfImageShown));
        }

        return indexOfImageShown;
    }

    private void setButtonStates()
    {
        if(indexOfImageShown == 0)
        {
            prv.setEnabled(false);
            nxt.setEnabled(true);
        }
        else if(indexOfImageShown == imageURLs.size()-1)
        {
            prv.setEnabled(true);
            nxt.setEnabled(false);
        }
        else
        {
            prv.setEnabled(true);
            nxt.setEnabled(true);
        }
    }


    public void onClick(View view) {
        Log.d(TAG, "onClick()");

        switch (view.getId()) {
            case R.id.my_previousDP:
                Log.d(TAG, "previous");
                indexOfImageShown = myPreviousImage();
                break;
            case R.id.my_nextDP:
                Log.d(TAG, "next");
                indexOfImageShown = myNextImage();
                break;
            case R.id.myAdDetailPageShare:
                Log.d(TAG, "share");
                shareAd();
                break;
            case R.id.myAdDetailPageDelete:
                Log.d(TAG, "delete");
                deleteAd();
                break;
            default:
                break;
        }

    }

    private  void deleteAd()
    {
        databaseReference.child("ads").child(key).child("isDeleted").setValue("true");

        Fragment myAdsFragment = new MyAdsFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, myAdsFragment, "findThisFragment")
                .addToBackStack(null).commit();
    }
    private void shareAd()
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = "A SapiAd was shared with You";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
        Log.d(TAG, "shareAd() called and ran");
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
