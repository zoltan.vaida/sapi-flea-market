package info.sapifleamarket.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.List;

@IgnoreExtraProperties
public class Ad implements Serializable {

    public static final String FIELD_TITLE = "Title";
    public static final String FIELD_DESCRIPTION = "Description";

    private String key;
    private String title;
    private String shortDescription;
    private String longDescription;
    private String phoneNumber;
    private String location;
    private String userID;
    private List<String> imageStorageIDs;
    private int numberOfView;
    private String imageURL;
    private String isDeleted;

    public Ad(){
        // Default constructor required for calls to DataSnapshot.getValue(Ad.class)
    }

    public Ad(String title, String sDescription, String lDescription, String phoneNumber, String location, String userID, List <String> imageStorageIDs, int numberOfView, String isDeleted) {
        this.title = title;
        this.shortDescription = sDescription;
        this.longDescription = lDescription;
        this.phoneNumber = phoneNumber;
        this.location = location;
        this.userID = userID;
        this.imageStorageIDs = imageStorageIDs;
        this.numberOfView=numberOfView;
        this.isDeleted = isDeleted;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Ad(String key, String title, String shortDescription, int numberOfView, String imageURL)
    {
        this.key = key;
        this.title=title;

        this.shortDescription=shortDescription;
        this.numberOfView=numberOfView;
        this.imageURL=imageURL;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public int getNumberOfView() {
        return numberOfView;
    }

    public void setNumberOfView(int numberOfView) {
        this.numberOfView = numberOfView;
    }

    public List <String> getImageStorageIDs() {
        return imageStorageIDs;
    }

    public void setImageStorageIDs(List <String> imageStorageIDs) {
        this.imageStorageIDs = imageStorageIDs;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public static String getFieldTitle() {
        return FIELD_TITLE;
    }

    public static String getFieldDescription() {
        return FIELD_DESCRIPTION;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String description) {
        this.shortDescription = description;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String description) {
        this.longDescription = description;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
