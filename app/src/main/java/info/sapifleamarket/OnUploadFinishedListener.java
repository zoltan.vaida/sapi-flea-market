package info.sapifleamarket;

import java.util.List;

interface OnUploadFinishedListener {
    void uploadFinished(List<String> uris);
}
