package info.sapifleamarket.adapter;

public interface AdListOnClickListener {
    void onItemClicked(String key);
}
