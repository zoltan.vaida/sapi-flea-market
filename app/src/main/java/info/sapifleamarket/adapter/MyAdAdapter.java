package info.sapifleamarket.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import info.sapifleamarket.R;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import info.sapifleamarket.model.Ad;


public class MyAdAdapter extends RecyclerView.Adapter<MyAdAdapter.MyViewHolder>
{
    private List<Ad> adList;

    private AdListOnClickListener listener;

    public MyAdAdapter(List<Ad> ads, AdListOnClickListener listener)
    {
        this.adList = ads;
        this.listener = listener;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder
    {

        public String key;
        //private ImageView images, seller, viewCounterIcon;
        public ImageView viewCounterIcon;
        public TextView title, shortDescription, viewCounter;
        public ImageView imageView;

        public MyViewHolder(View itemView)
        {
            super(itemView);
            //images = itemView.findViewById(R.id.image_switcher);
            //seller = itemView.findViewById(R.id.seller);
            title = itemView.findViewById(R.id.tv_adItemTitle);
            shortDescription = itemView.findViewById(R.id.tv_AdItemShortDescription);
            //viewCounterIcon = itemView.findViewById(R.id.);
            viewCounter = itemView.findViewById(R.id.adItemViewCounter);
            imageView = itemView.findViewById(R.id.image_view);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        listener.onItemClicked(key);
                    }
                }
            });
        }

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType)
    {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ad_item, viewGroup, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position)
    {
        holder.key = adList.get(position).getKey();
        holder.title.setText(adList.get(position).getTitle());
        holder.shortDescription.setText(adList.get(position).getShortDescription());
        holder.viewCounter.setText(String.valueOf(adList.get(position).getNumberOfView()));

        RequestOptions options = new RequestOptions();
        options.centerCrop();
        Glide.with(holder.viewCounter.getContext())
                .load(adList.get(position).getImageURL()).apply(options)
                .into(holder.imageView);

        //holder.bind(adList.get(position), listener);
    }

    @Override
    public int getItemCount()
    {
        return adList.size();
    }
}
